/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visual;
import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.view.JasperViewer;
import java.util.HashMap;
import java.io.File;
import java.sql.DriverManager;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
/**
 *
 * @author Riansyah
 */
public class Laporan_PesanJasa extends javax.swing.JInternalFrame {
private Connection conn = new Koneksi().connect();
private DefaultTableModel tabModel;
JasperReport jasper;
JasperDesign jasdes;
JasperPrint jasprint;


    /**
     * Creates new form Laporan_PesanJasa
     */
    public Laporan_PesanJasa() {
        initComponents();
         ((javax.swing.plaf.basic.BasicInternalFrameUI)getUI()).setNorthPane(null);
         data();
         isicombo1();
         kosong();
    }
private void kosong(){
   cpesan.setSelectedItem("");
}
protected void data(){
    DefaultTableModel model = new DefaultTableModel();
    model.addColumn("ID Pesanan");
    model.addColumn("ID Pelanggan");
    model.addColumn("ID Karyawan");
    model.addColumn("NamaPemesan");
    model.addColumn("Alamat");
    model.addColumn("No Telp");
    model.addColumn("JenisPesanan");
     model.addColumn("Harga");
    model.addColumn("TanggalPesan");
    
    try{
        String sql ="select * from pesanan";
        java.sql.Connection conn = (Connection)Koneksi.connect();
        java.sql.Statement stat=conn.createStatement();
        java.sql.ResultSet rs =stat.executeQuery(sql);
        while(rs.next()){
            model.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6),
                rs.getString(7), rs.getString(8), rs.getString(9)});
            
            }
        tablelaporanjasa.setModel(model);
        }catch(Exception e){     
    }
}
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txcari = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablelaporanjasa = new javax.swing.JTable();
        txprint = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        cpesan = new javax.swing.JComboBox();

        jPanel1.setBackground(new java.awt.Color(102, 102, 255));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/laporan.png"))); // NOI18N
        jLabel1.setText("LAPORAN PEMESANAN JASA");

        txcari.setBackground(new java.awt.Color(255, 255, 255));
        txcari.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        txcari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/cari.png"))); // NOI18N
        txcari.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        txcari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txcariActionPerformed(evt);
            }
        });

        jLabel2.setBackground(new java.awt.Color(255, 255, 255));
        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/cari.png"))); // NOI18N
        jLabel2.setText("Cari Berdasarkan ID Pesanan");

        tablelaporanjasa.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablelaporanjasa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablelaporanjasaMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablelaporanjasa);

        txprint.setBackground(new java.awt.Color(255, 255, 255));
        txprint.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        txprint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/print.jpg"))); // NOI18N
        txprint.setText("PRINT");
        txprint.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        txprint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txprintActionPerformed(evt);
            }
        });

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/batal-copy.png"))); // NOI18N
        jLabel3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel3MouseClicked(evt);
            }
        });

        cpesan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "PILIH", " " }));
        cpesan.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cpesan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cpesanActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(155, 155, 155)
                                .addComponent(jLabel2)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
                        .addComponent(cpesan, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txcari, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txprint)
                                .addGap(185, 185, 185))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addGap(25, 25, 25)))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(cpesan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(4, 4, 4))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txprint)
                        .addComponent(txcari, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(21, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txcariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txcariActionPerformed
        DefaultTableModel model = new DefaultTableModel();
    model.addColumn("ID Pesanan");
    model.addColumn("ID Pelanggan");
    model.addColumn("ID Karyawan");
    model.addColumn("Nama Pemesan");
    model.addColumn("Alamat");
    model.addColumn("No Telp");
    model.addColumn("Jenis Pesanan");
     model.addColumn("Harga Pesanan");
    model.addColumn("Tanggal Pesanan");
    
    try{
        Koneksi.connect();
        String sql ="select * from pesanan where id_pesanan like'%"+cpesan.getSelectedItem()+"%'";
        java.sql.Connection conn = (Connection)Koneksi.connect();
        java.sql.Statement stat=conn.createStatement();
        java.sql.ResultSet rs =stat.executeQuery(sql);
        while(rs.next()){
            model.addRow(new Object[]{rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6),
                rs.getString(7), rs.getString(8), rs.getString(9)});
            
            }
        tablelaporanjasa.setModel(model);
        }catch(Exception e){     
    }
    }//GEN-LAST:event_txcariActionPerformed

    private void jLabel3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseClicked
    this.dispose();
    }//GEN-LAST:event_jLabel3MouseClicked

    private void txprintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txprintActionPerformed
      try {
          String NamaFile = "src/Laporan/report1.jasper";
          String url = "jdbc:mysql://localhost:3306/";
          String db = "steamer";
          String driver = "com.mysql.jdbc.Driver";
          String user = "root";
          String pass = "";
          Class.forName("com.mysql.jdbc.Driver").newInstance();
          Connection koneksi = DriverManager.getConnection(url + db, user, pass);
          HashMap param = new HashMap();
          param.put("id_pesanan", String.valueOf(cpesan.getSelectedItem()));
          JasperPrint jasprint = JasperFillManager.fillReport(NamaFile, param, koneksi);
          JasperViewer.viewReport(jasprint, false);
          
      }catch (Exception ex){
          System.out.println(ex);
      }
    }//GEN-LAST:event_txprintActionPerformed
 public void isicombo1(){
    try{
      Statement statement = (Statement)Koneksi.connect().createStatement();
      ResultSet rs = statement.executeQuery("select * from pesanan");
      while(rs.next()){
          cpesan.addItem(rs.getString("id_pesanan"));
         
          
        
      }
      rs.close();
    }catch (Exception e){
      JOptionPane.showMessageDialog(rootPane, "Gagal");
    }
}
    private void cpesanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cpesanActionPerformed
        String vid = String.valueOf(cpesan.getSelectedItem());
        try {
            Statement statement = (Statement)Koneksi.connect().createStatement();
      ResultSet rs = statement.executeQuery("select * from pesanan where id_pesanan='"+vid+"'");
      while(rs.next()){
       
       vid=rs.getString("id_pesanan");
       cpesan.setSelectedItem(vid);
 }
      rs.close();
        }catch(Exception e){
        }  
                       
    }//GEN-LAST:event_cpesanActionPerformed

    private void tablelaporanjasaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablelaporanjasaMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tablelaporanjasaMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox cpesan;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablelaporanjasa;
    private javax.swing.JButton txcari;
    private javax.swing.JButton txprint;
    // End of variables declaration//GEN-END:variables
}
