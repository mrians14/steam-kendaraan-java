/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visual;
import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.JComboBox;
/**
 *
 * @author Riansyah
 */
public class Pesan_Jasa extends javax.swing.JInternalFrame {
private Connection conn = new Koneksi().connect();
private DefaultTableModel tabModel;
private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");
    /**
     * Creates new form Pesan_Jasa
     */
    public Pesan_Jasa() {
        initComponents();
         ((javax.swing.plaf.basic.BasicInternalFrameUI)getUI()).setNorthPane(null);
         dataTable();
         isicombo1();
         isicombo();
         kosong();
    }
protected void aktif(){
    txidpesan.setEnabled(true);
    txidpel.setEnabled(true);
    txidkar.setEnabled(true);
    txjenpes.requestFocus();
    txjenpes.setEnabled(true);
    txharga.setEnabled(true);
    }
    protected void kosong(){
    datepesan.setDate(null);
    txidpesan.setText("");
    txidpel.setSelectedIndex(0);
    txidkar.setSelectedIndex(0);
    txjenpes.setSelectedIndex(0);
    txharga.setText("");
    txnampes.setText("");
    txtelp.setText("");
    txalamat.setText("");
    cari.setText("");
    }
    protected void dataTable(){
    Object[] Baris = {"Id Pesanan", "Id Pelanggan","Id Karyawan","Nama Pemesanan", "No Telp", "Alamat","Jenis Pesanan","Harga","Tgl Pesan"};
    tabModel = new DefaultTableModel(null, Baris);
    String data = cari.getText();
    try{
        String query = "Select * from pesanan where id_pesanan like '%"+data+"%' order by id_pesanan asc";
        java.sql.Statement stat = conn.createStatement();
       ResultSet getdata = stat.executeQuery(query);
        while (getdata.next()){
            tabModel.addRow(new Object[]{
             getdata.getString(1),
             getdata.getString(2),
             getdata.getString(3),
             getdata.getString(4),
             getdata.getString(5),
             getdata.getString(6),
             getdata.getString(7),
             getdata.getString(8),
             getdata.getString(9)
            });
        }
        tablepesan.setModel(tabModel);
    }catch (Exception e){
        
    }
}
    public void isicombo1(){
        try{
        String sql = "select id_karyawan from karyawan order by id_karyawan";
        java.sql.Statement stat = conn.createStatement();
        ResultSet hasil = stat.executeQuery(sql);
        while(hasil.next()){
            Object[] ob = new Object[3];
            ob[0] = hasil.getString(1);
            
            txidkar.addItem((String) ob[0]);
        }
        hasil.close();
        stat.close();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
     public void isicombo(){
        try{
        String sql = "select id_pelanggan from pelanggan order by id_pelanggan";
        java.sql.Statement stat = conn.createStatement();
        ResultSet hasil = stat.executeQuery(sql);
        while(hasil.next()){
            Object[] ob = new Object[2];
            ob[0] = hasil.getString(1);
            
            txidpel.addItem((String) ob[0]);
        }
        hasil.close();
        stat.close();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txidpesan = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txidpel = new javax.swing.JComboBox();
        txidkar = new javax.swing.JComboBox();
        txjenpes = new javax.swing.JComboBox();
        txharga = new javax.swing.JTextField();
        txnampes = new javax.swing.JTextField();
        txtelp = new javax.swing.JTextField();
        datepesan = new com.toedter.calendar.JDateChooser();
        jScrollPane1 = new javax.swing.JScrollPane();
        txalamat = new javax.swing.JTextArea();
        sdata = new javax.swing.JButton();
        hdata = new javax.swing.JButton();
        tbatal = new javax.swing.JButton();
        edata1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        bcari = new javax.swing.JButton();
        cari = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        tablepesan = new javax.swing.JTable();
        jLabel11 = new javax.swing.JLabel();

        jPanel1.setBackground(new java.awt.Color(102, 102, 255));

        jPanel2.setBackground(new java.awt.Color(102, 102, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.white, java.awt.Color.white, java.awt.Color.white, java.awt.Color.white));

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("ID PESANAN");

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("ID PELANGGAN");

        jLabel4.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("NAMA PEMESAN");

        jLabel5.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("ID KARYAWAN");

        jLabel6.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("NO TELP PEMESAN");

        jLabel7.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("ALAMAT");

        jLabel8.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("JENIS PESANAN");

        jLabel9.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("HARGA PESANAN");

        jLabel10.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("TANGGAL PESANAN");

        txidpel.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "--Pilih Id Pelanggan--", " " }));
        txidpel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txidpelActionPerformed(evt);
            }
        });

        txidkar.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "--Pilih Id Karyawan--", " ", " " }));

        txjenpes.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "--Pilih--", "Cuci", "Ganti Oli", "Cuci dan Ganti Oli", "Cuci Helm", "Cuci Helm dan Cuci dan Ganti Oli" }));
        txjenpes.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        txjenpes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txjenpesActionPerformed(evt);
            }
        });

        txalamat.setColumns(20);
        txalamat.setRows(5);
        jScrollPane1.setViewportView(txalamat);

        sdata.setBackground(new java.awt.Color(255, 255, 255));
        sdata.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        sdata.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/simpan.jpg"))); // NOI18N
        sdata.setText("Simpan");
        sdata.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        sdata.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        sdata.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        sdata.setSelected(true);
        sdata.setVerifyInputWhenFocusTarget(false);
        sdata.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sdataMouseClicked(evt);
            }
        });

        hdata.setBackground(new java.awt.Color(255, 255, 255));
        hdata.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        hdata.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/hapus.png"))); // NOI18N
        hdata.setText("Hapus");
        hdata.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        hdata.setFocusTraversalPolicyProvider(true);
        hdata.setHideActionText(true);
        hdata.setHorizontalAlignment(javax.swing.SwingConstants.LEADING);
        hdata.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        hdata.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                hdataMouseClicked(evt);
            }
        });
        hdata.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hdataActionPerformed(evt);
            }
        });

        tbatal.setBackground(new java.awt.Color(255, 255, 255));
        tbatal.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        tbatal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/batal-copy.png"))); // NOI18N
        tbatal.setText("Batal");
        tbatal.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tbatal.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        tbatal.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbatalMouseClicked(evt);
            }
        });
        tbatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tbatalActionPerformed(evt);
            }
        });

        edata1.setBackground(new java.awt.Color(255, 255, 255));
        edata1.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        edata1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/edit.jpg"))); // NOI18N
        edata1.setText("Edit");
        edata1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        edata1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        edata1.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        edata1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                edata1MouseClicked(evt);
            }
        });
        edata1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edata1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel5)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel7))
                .addGap(30, 30, 30)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)
                    .addComponent(txidkar, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txidpel, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txidpesan)
                    .addComponent(txnampes, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(47, 47, 47)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(43, 43, 43)
                                .addComponent(hdata))
                            .addComponent(jLabel9))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(sdata)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(edata1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tbatal))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(txharga, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel8))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtelp, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txjenpes, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(datepesan, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(59, 59, 59))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txidpesan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(txtelp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txjenpes, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txharga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txidpel, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txidkar, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5)
                            .addComponent(jLabel10))
                        .addGap(22, 22, 22)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txnampes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4)))
                    .addComponent(datepesan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(hdata)
                        .addComponent(sdata)
                        .addComponent(edata1)
                        .addComponent(tbatal))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addContainerGap(18, Short.MAX_VALUE))
        );

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/Pemesanan.jpg"))); // NOI18N
        jLabel1.setText("DATA PEMESANAN JASA");

        bcari.setBackground(new java.awt.Color(255, 255, 255));
        bcari.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        bcari.setText("Cari");
        bcari.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bcari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bcariActionPerformed(evt);
            }
        });

        tablepesan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablepesan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablepesanMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tablepesan);

        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/batal-copy.png"))); // NOI18N
        jLabel11.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel11MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane3)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(bcari)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cari, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 618, Short.MAX_VALUE))
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 861, Short.MAX_VALUE))
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel11)
                        .addGap(24, 24, 24))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel11))
                .addGap(13, 13, 13)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bcari)
                    .addComponent(cari, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(46, 46, 46))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void hdataMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_hdataMouseClicked
        int ok = JOptionPane.showConfirmDialog(null, "Hapus","Konfirmasi Dialog", JOptionPane.YES_NO_CANCEL_OPTION);
       if (ok ==0){
           String sql = "delete from pesanan where id_pesanan='"+txidpesan.getText()+"'";
           try{
               PreparedStatement stat = conn.prepareStatement(sql);
               stat.executeUpdate();
               JOptionPane.showMessageDialog(null, "Berhasil Dihapus");
               kosong();
               txidpesan.requestFocus();
               dataTable();
           }catch (SQLException t){
               JOptionPane.showMessageDialog(null, "Gagal Hapus");
           }
       }
    }//GEN-LAST:event_hdataMouseClicked

    private void hdataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hdataActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_hdataActionPerformed

    private void txjenpesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txjenpesActionPerformed
         if (txjenpes.getSelectedItem().equals("Cuci")){
txharga.setText("15000");
}
if (txjenpes.getSelectedItem().equals("Ganti Oli")){
txharga.setText("20000");
}
if (txjenpes.getSelectedItem().equals("Cuci dan Ganti Oli")){
txharga.setText("30000");
}
if (txjenpes.getSelectedItem().equals("Cuci Helm")){
txharga.setText("10000");
}
if (txjenpes.getSelectedItem().equals("Cuci Helm dan Cuci dan Ganti Oli")){
txharga.setText("50000");
}
                        
    }//GEN-LAST:event_txjenpesActionPerformed

    private void sdataMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sdataMouseClicked
        String sql = "insert into pesanan values (?,?,?,?,?,?,?,?,?)";
      try{
          PreparedStatement stat = conn.prepareStatement(sql);
          stat.setString(1, txidpesan.getText());
          stat.setString(2, txidpel.getSelectedItem().toString());
          stat.setString(3, txidkar.getSelectedItem().toString());
          stat.setString(4, txnampes.getText());
          stat.setString(5, txtelp.getText());
          stat.setString(6, txalamat.getText());
          stat.setString(7, txjenpes.getSelectedItem().toString());
          stat.setString(8, txharga.getText());
          String date = String.valueOf(sdf.format(datepesan.getDate()));
          stat.setString(9, date);
          
          stat.executeUpdate();
          JOptionPane.showMessageDialog(null, "Data Disimpan");
          kosong();
          txidpesan.requestFocus();
          dataTable();
      }catch (SQLException e){
          JOptionPane.showMessageDialog(null, "Data Gagal Disimpan");
      }
    }//GEN-LAST:event_sdataMouseClicked

    private void bcariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bcariActionPerformed
          Object [] baris ={"Id Pesanan","Id Pelanggan","Id Karyawan","Nama ","No Telp","Alamat","Jenis Pesanan","harga","Tanggal Pesan"};
        tabModel = new DefaultTableModel (null,baris);
        tablepesan.setModel(tabModel);
        String sql ="select * from pesanan where id_pesanan like'%"+cari.getText()+"%' or nama_pesanan like'%"+cari.getText()+"'";
        try{
            java.sql.Statement stat = conn.createStatement();
            ResultSet hasil = stat.executeQuery(sql);
            while (hasil.next()){
            String r = hasil.getString("id_pesanan");
            String i = hasil.getString ("id_pelanggan");
            String a = hasil.getString ("id_karyawan");
            String n = hasil.getString ("nama_pemesan");
            String y = hasil.getString ("notlp_pemesan");
            String h = hasil.getString ("alamat");
            String m = hasil.getString ("jenis_pesanan");
            String d = hasil.getString("harga_pesanan");
            String u = hasil.getString("tanggal_pesan");
            String [] data = {r,i,a,n,y,h,m,d,u};
            tabModel.addRow (data);
        
        }
        }catch (Exception e ){
            System.out.println(e);
        } 
    }//GEN-LAST:event_bcariActionPerformed

    private void tbatalMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbatalMouseClicked
        kosong();
        dataTable();
    }//GEN-LAST:event_tbatalMouseClicked

    private void edata1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edata1ActionPerformed
        try {
            String sql ="update pesanan set  id_pelanggan=?, id_karyawan=?, nama_pemesan=?, notelp_pemesan=?, alamat=?, jenis_pesanan=?, harga_pesanan=?, tanggal_pesan=? where id_pesanan=?";
            PreparedStatement stat = conn.prepareStatement(sql);
            stat.setString(1, txidpel.getSelectedItem().toString());
            stat.setString(2, txidkar.getSelectedItem().toString());
            stat.setString(3, txnampes.getText());
            stat.setString(4, txtelp.getText());
            stat.setString(5, txalamat.getText());
            stat.setString(6, txjenpes.getSelectedItem().toString());
            stat.setString(7, txharga.getText());
            String date = String.valueOf(sdf.format(datepesan.getDate()));
            stat.setString(8, date);
             stat.setString(9, txidpesan.getText());

            stat.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data Diubah");
            kosong();
            txidpesan.requestFocus();
            dataTable();
            cari.setText("");
            
        }catch (SQLException e){
            JOptionPane.showMessageDialog(null, "Gagal diubah");
        }
    }//GEN-LAST:event_edata1ActionPerformed

    private void edata1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_edata1MouseClicked

    }//GEN-LAST:event_edata1MouseClicked

    private void tablepesanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablepesanMouseClicked
       try {   
        int bar = tablepesan.getSelectedRow();
        String r = tabModel.getValueAt(bar, 0).toString();
        String i = tabModel.getValueAt(bar, 1).toString();
        String a = tabModel.getValueAt(bar, 2).toString();
        String n = tabModel.getValueAt(bar, 3).toString();
        String y = tabModel.getValueAt(bar, 4).toString();
        String h = tabModel.getValueAt(bar, 5).toString();
        String m = tabModel.getValueAt(bar, 6).toString();
        String d = tabModel.getValueAt(bar, 7).toString();      
       java.util.Date date = sdf.parse((String)tabModel.getValueAt(bar, 8));
        java.sql.Date uDate = new java.sql.Date(date.getTime());
        
       
        datepesan.setDate(uDate);
        txidpesan.setText(r);
        txidpel.setSelectedItem(i);
        txidkar.setSelectedItem(a);
        txnampes.setText(n);
        txalamat.setText(y);
        txtelp.setText(h);
        txjenpes.setSelectedItem(m);
        txharga.setText(d);
        } catch (ParseException ex) {
        Logger.getLogger(Laporan_PesanJasa.class.getName()).log(Level.SEVERE, null, ex);
    }
    }//GEN-LAST:event_tablepesanMouseClicked

    private void tbatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tbatalActionPerformed
       kosong();
       dataTable();
    }//GEN-LAST:event_tbatalActionPerformed

    private void jLabel11MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel11MouseClicked
      dispose();
    }//GEN-LAST:event_jLabel11MouseClicked

    private void txidpelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txidpelActionPerformed
      String vnam = String.valueOf(txnampes.getText());
String vid = String.valueOf(txidpel.getSelectedItem());
        try {
            Statement statement = (Statement)Koneksi.connect().createStatement();
      ResultSet rs = statement.executeQuery("select * from pelanggan where id_pelanggan='"+vid+"'");
      while(rs.next()){
       vnam=rs.getString("nama_pelanggan");
       vid=rs.getString("id_pelanggan");
       txidpel.setSelectedItem(vid);
       txnampes.setText(vnam);
      }
      rs.close();
        }catch(Exception e){
        }    
    }//GEN-LAST:event_txidpelActionPerformed
private void formWindowActivated(java.awt.event.WindowEvent evt) {
// TODO add your handling code here:
txjenpes.addItem("");
txjenpes.addItem("Cuci");
txjenpes.addItem("Ganti Oli");
txjenpes.addItem("Cuci dan Ganti Oli");
txjenpes.addItem("Cuci Helm ");
txjenpes.addItem("Cuci Helm dan Cuci dan Ganti Oli");
}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bcari;
    private javax.swing.JTextField cari;
    private com.toedter.calendar.JDateChooser datepesan;
    private javax.swing.JButton edata1;
    private javax.swing.JButton hdata;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JButton sdata;
    private javax.swing.JTable tablepesan;
    private javax.swing.JButton tbatal;
    private javax.swing.JTextArea txalamat;
    private javax.swing.JTextField txharga;
    private javax.swing.JComboBox txidkar;
    private javax.swing.JComboBox txidpel;
    private javax.swing.JTextField txidpesan;
    private javax.swing.JComboBox txjenpes;
    private javax.swing.JTextField txnampes;
    private javax.swing.JTextField txtelp;
    // End of variables declaration//GEN-END:variables
}
