-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 29, 2020 at 06:52 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `steamer`
--

-- --------------------------------------------------------

--
-- Table structure for table `bayar`
--

CREATE TABLE IF NOT EXISTS `bayar` (
  `id_pembayaran` varchar(20) NOT NULL DEFAULT '',
  `id_pesanan` varchar(20) NOT NULL,
  `id_pelanggan` varchar(20) NOT NULL,
  `id_karyawan` varchar(20) NOT NULL,
  `nama_karyawan` varchar(50) NOT NULL,
  `nama_pelanggan` varchar(50) NOT NULL,
  `jenis_pesanan` varchar(20) NOT NULL,
  `tanggal_pesanan` varchar(25) NOT NULL,
  `total_bayar` varchar(50) NOT NULL,
  PRIMARY KEY (`id_pembayaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bayar`
--

INSERT INTO `bayar` (`id_pembayaran`, `id_pesanan`, `id_pelanggan`, `id_karyawan`, `nama_karyawan`, `nama_pelanggan`, `jenis_pesanan`, `tanggal_pesanan`, `total_bayar`) VALUES
('11111', '777', '38C', '98HG', 'Yunusa', 'Yuki', 'Cuci', '11-06-2020', '15000');

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE IF NOT EXISTS `karyawan` (
  `id_karyawan` varchar(20) NOT NULL DEFAULT '',
  `nama_karyawan` varchar(50) NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `tanggal_lahir` varchar(25) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  PRIMARY KEY (`id_karyawan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`id_karyawan`, `nama_karyawan`, `jenis_kelamin`, `tanggal_lahir`, `no_hp`, `alamat`) VALUES
('123AA', 'Jaka', 'Laki-Laki', '12-06-2020', '088212913180', 'Jalan Baru'),
('123BB', 'Yuli', 'Perempuan', '26-01-2020', '088913414589', 'Jalan Lama'),
('98HG', 'Yunusa', 'Perempuan', '18-01-2020', '088128471741', 'Yuk Jalan'),
('Y3CF', 'Handnad', 'Laki-Laki', '12-06-2020', '088948242256', 'Jakarta');

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE IF NOT EXISTS `pelanggan` (
  `id_pelanggan` varchar(20) NOT NULL DEFAULT '',
  `nama_pelanggan` varchar(50) NOT NULL,
  `jenkel` varchar(10) NOT NULL,
  `tanggal_lahir` varchar(20) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  PRIMARY KEY (`id_pelanggan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`id_pelanggan`, `nama_pelanggan`, `jenkel`, `tanggal_lahir`, `no_hp`, `alamat`) VALUES
('36B', 'Yuli', 'Perempuan', '14-07-2020', '08891737189', 'Jalan Baru Enak Uh'),
('38C', 'Yuki', 'Laki-Laki', '17-06-2020', '08891471414', 'Enak Jalan'),
('40D', 'Yeni', 'Perempuan', '12-06-1997', '081873616415', 'Jalan Lama');

-- --------------------------------------------------------

--
-- Table structure for table `pesanan`
--

CREATE TABLE IF NOT EXISTS `pesanan` (
  `id_pesanan` varchar(20) NOT NULL DEFAULT '',
  `id_pelanggan` varchar(20) NOT NULL,
  `id_karyawan` varchar(20) NOT NULL,
  `nama_pemesan` varchar(50) NOT NULL,
  `notelp_pemesan` varchar(15) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `jenis_pesanan` varchar(20) NOT NULL,
  `harga_pesanan` varchar(50) NOT NULL,
  `tanggal_pesan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_pesanan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesanan`
--

INSERT INTO `pesanan` (`id_pesanan`, `id_pelanggan`, `id_karyawan`, `nama_pemesan`, `notelp_pemesan`, `alamat`, `jenis_pesanan`, `harga_pesanan`, `tanggal_pesan`) VALUES
('111', '38C', '123BB', 'Jana', '088716361', 'Jalanan Sini', 'Cuci dan Ganti Oli', '30000', '10-06-2020'),
('222', '40D', '98HG', 'Yuna', '088716314', 'Jalanan Baru', 'Cuci Helm', '10000', '02-06-2020');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `password`) VALUES
('rian', '123');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
